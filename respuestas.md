## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?:1268
* ¿Cuánto tiempo dura la captura?:12.81 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?:100
* ¿Qué protocolos de nivel de red aparecen en la captura?:IPv4
* ¿Qué protocolos de nivel de transporte aparecen en la captura?:UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?:RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N):133
* Dirección IP de la máquina A:216.234.64.16
* Dirección IP de la máquina B:192.168.0.10
* Puerto UDP desde el que se envía el paquete:54550
* Puerto UDP hacia el que se envía el paquete:49154
* SSRC del paquete:0x31be1e0e
* Tipo de datos en el paquete RTP (según indica el campo correspondiente):ITU-T G.711 PCMU (0)
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?:7
* ¿Cuántos paquetes hay en el flujo F?:642
* ¿El origen del flujo F es la máquina A o B?:B
* ¿Cuál es el puerto UDP de destino del flujo F?:54550
* ¿Cuántos segundos dura el flujo F?:12.81
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?:31.653 ms
* ¿Cuál es el jitter medio del flujo?:12.234262
* ¿Cuántos paquetes del flujo se han perdido?:0
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?:12.81 - 0.03 segundos, practicamente todo el tiempo, menos el tiempo entre el primer y el segundo paquete
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?:18502
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?:Ha llegado tarde, tiene skew alto y positivo
* ¿Qué jitter se ha calculado para ese paquete?:0.329394 ms
* ¿Qué timestamp tiene ese paquete?:1769316203
* ¿Por qué número hexadecimal empieza sus datos de audio?:B
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`:mar 24 oct 2023 08:07:07 CEST
* Número total de paquetes en la captura:2626 paquetes
* Duración total de la captura (en segundos):8.901754919 s
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?:450 paquetes RTP
* ¿Cuál es el SSRC del flujo?:0xe6ac9de6
* ¿En qué momento (en ms) de la traza comienza el flujo?:2726.573 ms
* ¿Qué número de secuencia tiene el primer paquete del flujo?475
* ¿Cuál es el jitter medio del flujo?:0 (los paquetes no se están recibiendo, "port unreacheable")
* ¿Cuántos paquetes del flujo se han perdido?:0 paquetes
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?:
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura):
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?:
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?:
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?:
